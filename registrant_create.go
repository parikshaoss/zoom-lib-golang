package zoom

import (
	"fmt"
	"strconv"
)

// CreateRegistrantOptions are the options to create a registrant for a meeting with
type CreateRegistrantOptions struct {
	MeetingID int    `json:"-"`
	Email     string `json:"email,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Phone     string `json:"phone,omitempty"`
}

// CreateRegistrantPath - v2 create a registrant for a meeting
const CreateRegistrantPath = "/meetings/%s/registrants"

// CreateRegistrant calls POST /meetings/{meetingId}/registrants
func CreateRegistrant(opts CreateRegistrantOptions) (Registrant, error) {
	return defaultClient.CreateRegistrant(opts)
}

// CreateRegistrant calls POST /meetings/{meetingId}/registrants

func (c *Client) CreateRegistrant(opts CreateRegistrantOptions) (Registrant, error) {
	var ret = Registrant{}
	return ret, c.requestV2(requestV2Opts{
		Method:         Post,
		Path:           fmt.Sprintf(CreateRegistrantPath, strconv.Itoa(opts.MeetingID)),
		DataParameters: &opts,
		Ret:            &ret,
	})
}
