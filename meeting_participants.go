package zoom

import (
	"fmt"
	"strconv"
)

// CreateRegistrantOptions are the options to create a registrant for a meeting with
type MeetingParticipantsOptions struct {
	MeetingID int    `url:"-"`
	Type      string `url:"type,omitempty"`
	PageSize  int    `url:"page_size,omitempty"`
}

// CreateRegistrantPath - v2 create a registrant for a meeting
const MeetingParticipantsPath = "/metrics/meetings/%s/participants"

// CreateRegistrant calls POST /meetings/{meetingId}/registrants
func MeetingParticipants(opts MeetingParticipantsOptions) (Participants, error) {
	return defaultClient.MeetingParticipants(opts)
}

// CreateRegistrant calls POST /metrics/meetings/{meetingId}/participants

func (c *Client) MeetingParticipants(opts MeetingParticipantsOptions) (Participants, error) {
	var ret = Participants{}
	return ret, c.requestV2(requestV2Opts{
		Method:        Get,
		Path:          fmt.Sprintf(MeetingParticipantsPath, strconv.Itoa(opts.MeetingID)),
		URLParameters: &opts,
		Ret:           &ret,
	})
}
