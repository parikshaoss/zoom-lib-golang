package zoom

import "fmt"

// UpdateUserPath - v2 path for deleting a user
const UpdateUserPath = "/users/%s"

// UpdateUserOptions are the options to delete a user with
type UpdateUserOptions struct {
	EmailOrID string `json:"-"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
}

// UpdateUser calls PATCH /users/{userID}
func UpdateUser(opts UpdateUserOptions) error {
	return defaultClient.UpdateUser(opts)
}

// UpdateUser calls PATCH /users/{userID}
// https://marketplace.zoom.us/docs/api-reference/zoom-api/users/userdelete
func (c *Client) UpdateUser(opts UpdateUserOptions) error {
	return c.requestV2(requestV2Opts{
		Method:         Patch,
		Path:           fmt.Sprintf(UpdateUserPath, opts.EmailOrID),
		DataParameters: &opts,
		HeadResponse:   true,
	})
}
