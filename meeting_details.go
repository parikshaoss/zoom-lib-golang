package zoom

import "fmt"

// GetMeetingDetailsOptions are the options to get a meeting details
type GetMeetingDetailsOptions struct {
	MeetingID string `url:"-"`
	// Type      string `url:"type,omitempty"`
}

// MeetingDetailsPath - v2 get meeting details for past/live meeting
const MeetingDetailsPath = "/past_meetings/%s"

// GetMeetingDetails calls Get /past_meetings/{meetingId}
func GetMeetingDetails(opts GetMeetingDetailsOptions) (MeetingDetails, error) {
	return defaultClient.GetMeetingDetails(opts)
}

// GetMeetingDetails calls Get /past_meetings/{meetingId}
func (c *Client) GetMeetingDetails(opts GetMeetingDetailsOptions) (MeetingDetails, error) {
	var ret = MeetingDetails{}
	return ret, c.requestV2(requestV2Opts{
		Method:         Get,
		Path:           fmt.Sprintf(MeetingDetailsPath, opts.MeetingID),
		DataParameters: &opts,
		Ret:            &ret,
	})
}
